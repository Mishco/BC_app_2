# SKOPIROVAT CELY PRIECINOK

# BC_app_2 - CLIENT SIDE

Aplikácia Shared Mobile SMS - klientská časť
Min. verzia JAVA: jre 1.8 a vyššie  http://www.oracle.com/technetwork/java/javase/downloads/jre8-downloads-2133155.html

Inštálacia nie je potrebná.
Stačí skopírovať a spustiť.


Serverová aplikácia, dostupná na Google Play
https://play.google.com/store/apps/details?id=com.bc.michal.sms_chatpc

FAQ: 

1. Nenadviazanie žiadneho spojenia, aplikácia mrzne, ale mobil píše SPOJENÉ
Na Windowse pomáha vypnúť FireWall, alebo povoliť program vo sieťových výnimkách


2. Nevidím žiadne výpisy:
V prípade, že chcete vidieť výpisy toho čo sa v aplikácií deje, treba spustiť cez príkazová riadok:
cmd
java -jar "cesta k suboru.jar" (napr:  java -jar "D:\BC_app_client_1.0.jar")


UPOZORNENIE:
SMS spravy, ktore odosielate nie su bezplatne!

